# Earthdata Drupal 9 Database Dropbox

## Usage

To import a pre-populated database when first building the Docker stack, 
place a MySQL/MariaDB SQL dump file (.sql) into this directory.

This process will only run once. After that, the 'databases/drupal' directories would
need to be completely removed to make it executable once again.
