# Earthdata Drupal 9 Website Settings Dropbox

## Usage

Place the 'settings.php' and 'settings.local.php' files into this directory. The files will be 
automatically be moved to 'drupal/web/sites/default/' during the build process.
