# Earthdata Drupal Docker

## Contents of this File

* Overview
* Requirements
* Installation
* Troubleshooting
* More Information
* Maintainers/Support

## Overview

A complete Drupal-based Docker stack running the following primary containers:

* Nginx
* PHP
* MariaDB
* phpMyAdmin
* Mailhog
* Portainer
* Traefik

Optional, yet helpful containers:

* Crond
* Redis
* Solr
* Xhprof

Other available containers:

* PostgreSQL
* Apache
* Varnish
* Adminer
* Drupal Node
* Memcached
* Rsyslog
* AthenaPDF
* Node
* Blackfire
* Webgrind
* Elasticsearch
* Kibana
* OpenSMTPD
* Chrome

## Requirements

* [Docker Desktop](https://www.docker.com/products/docker-desktop)

## Installation

### Download

Download the [Latest release](https://bitbucket.org/ghalusa/earthdata-drupal-docker/downloads/earthdata-drupal-docker-latest.zip).

### Unzip

* Extract the `earthdata-drupal-docker` directory.
* From a command prompt, move into the `earthdata-drupal-docker` directory.

```
unzip earthdata-drupal-docker-latest.zip
cd earthdata-drupal-docker
```

### Drupal Database

* Place a MySQL/MariaDB SQL dump file (.sql) into the 'mariadb-init' directory. The database will be 
created when first building the Docker stack. ([Contact Gor](mailto:goran.halusa@nasa.gov) to obtain 
an up-to-date SQL dump file.)

* If a SQL dump file isn't placed into this directory, Drupal will be built from scratch.

### Drupal Settings

* Place the 'settings.php' and 'settings.local.php' files into this directory. These files will be 
automatically be moved to the target directory, 'drupal/web/sites/default/', during the build process. Alternatively, the settings files can be also placed directly into the target directory. ([Contact Gor](mailto:goran.halusa@nasa.gov) to obtain these files.)

### Run docker-compose

```
docker-compose up -d
```

### Build

Run the build.sh script from the command line:

```
./build.sh
```

### Setup Hosts

Open your `hosts` file:

```
sudo vim /etc/hosts
```

Add the following to the `hosts` file:

```
# Earthdata local Docker environment
127.0.0.1 earthdata.docker.localhost
127.0.0.1 solr.earthdata.docker.localhost
127.0.0.1 pma.earthdata.docker.localhost
127.0.0.1 mailhog.earthdata.docker.localhost
127.0.0.1 portainer.earthdata.docker.localhost
```

### Load the Drupal Website

[http://earthdata.docker.localhost:8001/](http://earthdata.docker.localhost:8001/)

### Post Installation Checks

* Check the ["Status report"](http://earthdata.docker.localhost:8001/admin/reports/status) page and make sure everything is in order. There may be some warnings, but pay more attention to errors. If errors are reported,
please feel free to submit an issue.

* Check the ["Recent log messages"](http://earthdata.docker.localhost:8001/admin/reports/dblog) page to ensure 
there are no major issues being reported.

----

## Troubleshooting

#### Loading the Website Generates a 404

* Go to [Portainer](http://portainer.earthdata.docker.localhost:8001/#/stacks/earthdata-drupal-docker?type=2&external=true).
* Select the Nginx container (earthdata_nginx) if it's stopped.
* Click the 'Start' button.
* Try loading the website again: [http://earthdata.docker.localhost:8001/](http://earthdata.docker.localhost:8001/)

----

## More Information

More information for topics not covered by these instructions can be found here:

https://wodby.com/docs/1.0/stacks/drupal/local/

## Maintainers/Support

* Goran "Gor" Halusa: [goran.halusa@nasa.gov](mailto:goran.halusa@nasa.gov)