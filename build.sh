#!/bin/sh

echo "\n================================"
echo "Earthdata Drupal 9 Installation"
echo "================================\n"

sleep 3

# Drupal has already been installed.
if [ -e "./drupal/composer.json" ] && [ -e "./drupal/.gitignore" ]
then
    echo "==================================="
    echo "Drupal has already been installed!"
    echo "===================================\n"
fi

# Drupal has not been installed.
if [ ! -e "./drupal/composer.json" ] && [ ! -e "./drupal/.gitignore" ]
then

    echo "Initializing..."

    # Make sure the drupal directory is empty.
    if [ -e "./drupal/.DS_Store" ]
    then
        rm ./drupal/.DS_Store
    fi

    sleep 2

    # Clone the 'earthdata-drupal-9' Git repo.
    echo "\nCloning the 'earthdata-drupal-9' Git repository...\n"
    git clone https://bitbucket.org/ghalusa/earthdata-drupal-9.git ./drupal/ && echo "\n'earthdata-drupal-9' has been successfully cloned.\n"

    sleep 2

    # Run 'composer install'
    echo "Running 'composer install'...\n"
    cd drupal/
    composer install && cd .. && echo "\n'composer install' completed.\n"

    sleep 3

    # Copy the settings.php and settings.local.php files to /drupal/web/sites/default.
    if [ ! -e "./drupal/web/sites/default/settings.php" ]
    then

        echo "Copying settings.php into web/sites/default/..."

        if [ -e "./drupal_settings/settings.php" ]
        then
            cp ./drupal_settings/settings.php ./drupal/web/sites/default/settings.php
            sleep 1
        else
            echo "\nThe settings.php file was not found."
            echo "If you have the settings.php file, please move it into web/sites/default/."
            echo "For further assistance, please email Goran Halusa at goran.halusa@nasa.gov.\n"
        fi
    fi

    if [ ! -e "./drupal/web/sites/default/settings.local.php" ]
    then

        echo "Copying settings.local.php into web/sites/default/..."

        if [ -e "./drupal_settings/settings.local.php" ]
        then
            cp ./drupal_settings/settings.local.php ./drupal/web/sites/default/settings.local.php
            sleep 1
        else
            echo "\nThe settings.local.php file was not found."
            echo "If you have the settings.local.php file, please move it into web/sites/default/."
            echo "For further assistance, please email Goran Halusa at goran.halusa@nasa.gov.\n"
        fi
    fi

    sleep 1

    # Create the files and private directories.
    echo "\nCreating the 'files' and 'private' directories..."
    cd ./drupal/web/sites/default/
    mkdir -p files/private

    sleep 2

    # Clear Drupal's caches.
    echo "\nClearing Drupal's caches (drush cr)...\n"
    docker-compose exec php drush cr && echo "\nDrupal's caches have been cleared."

    sleep 1

    # Run database updates.
    echo "\nUpdating Drupal's database (drush updb)...\n"
    docker-compose exec php drush updb -y && echo "\nDrupal's database has been updated."

    sleep 2

    echo "\n========================================"
    echo "All set!"
    echo "Drupal has been successfully installed!"
    echo "========================================\n"

fi

sleep 1

echo "Visit your Drupal instance here: http://earthdata.docker.localhost:8001\n"
